﻿namespace LibFsTransAid.ArchiveIO
{
    using FsFontSchema;
    using UndertaleModLib;
    using UndertaleModLib.Models;
    using UndertaleModLib.Util;

    public class FsAssetDao
    {
        public static void LoadFromFile(string path)
        {
            using (var fsr = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var assetFile = UndertaleIO.Read(fsr);
                Console.WriteLine($"Fonts.Count({assetFile.Fonts.Count})");
                foreach (var font in assetFile.Fonts)
                {
                    Console.WriteLine($"Name({font.Name}) DisplayName({font.DisplayName}) ");
                    ////using (var fsw = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
                    ////{
                    ////    var undertaleWriter = new UndertaleWriter(fsw);
                    ////    font.Serialize(undertaleWriter);
                    ////}
                }
            }
        }

        /// <summary>
        /// データファイル(data.win)にPNGファイルを追加する。
        /// </summary>
        /// <param name="datawinSrcPath">オリジナルのデータファイル</param>
        /// <param name="datawinDstPath">出力用データファイル</param>
        /// <param name="pngPath">PNGファイルのパス</param>
        public static void AddTexture(string datawinSrcPath, string datawinDstPath, string pngPath)
        {
            //// Embedded Texture を作成
            var embeddedTexture = new UndertaleEmbeddedTexture();
            //// PNG形式の画像ファイルを読み込み
            using (var ms = new MemoryStream(TextureWorker.ReadTextureBlob(pngPath)))
            {
                //// 読み込んだ画像データを取り込み
                embeddedTexture.TextureData.TextureBlob = ms.ToArray();
            }

            //// オリジナルのデータファイルを開く
            using (var fsr = new FileStream(datawinSrcPath, FileMode.Open, FileAccess.Read))
            {
                //// オリジナルのデータファイルを読み込み
                var undertaleData = UndertaleIO.Read(fsr);
                //// データファイルに Texture を追加する
                undertaleData.EmbeddedTextures.Add(embeddedTexture);
                //// 出力用データファイルに書き出す
                var fsw = new FileStream(datawinDstPath, FileMode.OpenOrCreate, FileAccess.Write);
                UndertaleIO.Write(fsw, undertaleData, x =>
                {
                    Console.WriteLine(x);
                });
            }
        }

        public static void ExportFont(string datawinPath, string fontName, string outFolderPath)
        {
            using (var fsr = new FileStream(datawinPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var assetFile = UndertaleIO.Read(fsr);
                Console.WriteLine($"Fonts.Count({assetFile.Fonts.Count})");
                foreach (var font in assetFile.Fonts)
                {
                    if (font.Name.Content.Equals(fontName, StringComparison.OrdinalIgnoreCase))
                    {
                        var pngPath = $"{Path.Combine(outFolderPath, fontName)}.png";
                        File.WriteAllBytes(pngPath, font.Texture.TexturePage.TextureData.TextureBlob);
                        var fontAtlas = new FsFontAtlas();
                        fontAtlas.Id = Guid.NewGuid();
                        fontAtlas.ModelName = "GMFont";
                        fontAtlas.Mvc = "1.1";
                        fontAtlas.Name = font.Name.Content;
                        fontAtlas.AntiAlias = font.AntiAliasing;
                        fontAtlas.TtfName = string.Empty;
                        fontAtlas.AscenderOffset = font.AscenderOffset;
                        fontAtlas.Bold = font.Bold;
                        fontAtlas.Charset = font.Charset;
                        fontAtlas.First = 0;
                        fontAtlas.FontName = font.DisplayName.Content;
                        fontAtlas.GlyphOperations = 0;
                        fontAtlas.Glyphs = new List<Glyph>();
                        fontAtlas.KerningPairs = new List<object>();
                        fontAtlas.Ranges = new List<Range>();
                        fontAtlas.SampleText = string.Empty;
                        fontAtlas.StyleName = "regular";
                        fontAtlas.Size = font.EmSize;
                        fontAtlas.TextureGroupId = Guid.NewGuid();
                        foreach (var orgGlyph in font.Glyphs)
                        {
                            if (orgGlyph != null)
                            {
                                var glyph = new Glyph();
                                glyph.Key = orgGlyph.Character;
                                glyph.Value = new Value();
                                glyph.Value.Id = Guid.NewGuid();
                                glyph.Value.ModelName = ModelName.GmGlyph;
                                glyph.Value.Mvc = "1.0";
                                glyph.Value.Character = orgGlyph.Character;
                                glyph.Value.H = orgGlyph.SourceHeight;
                                glyph.Value.Offset = orgGlyph.Offset;
                                glyph.Value.Shift = orgGlyph.Shift;
                                glyph.Value.W = orgGlyph.SourceWidth;
                                glyph.Value.X = orgGlyph.SourceX;
                                glyph.Value.Y = orgGlyph.SourceY;

                                fontAtlas.Glyphs.Add(glyph);
                            }
                        }

                        Converter.Settings.Formatting = Newtonsoft.Json.Formatting.Indented;
                        var jsonString = Serialize.ToJson(fontAtlas);
                        var jsonPath = $"{Path.Combine(outFolderPath, fontName)}.json";
                        File.WriteAllText(jsonPath, jsonString);
                        ////using (var fsw = new FileStream(outPath, FileMode.OpenOrCreate, FileAccess.Write))
                        ////using (var undertaleWriter = new UndertaleWriter(fsw))
                        ////{
                        ////    ////font.Serialize(undertaleWriter);
                        ////    ////font.Texture.Serialize(undertaleWriter);
                        ////}
                    }
                }
            }
        }
    }
}