﻿namespace LibFsTransAid.ArchiveIO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FsFontSchema;
    using LibFsTransAid.Utils;
    using UndertaleModLib;
    using UndertaleModLib.Models;
    using UndertaleModLib.Util;

    public class GmsAssets
    {
        public GmsAssets(string assetPath)
        {
            using (var fsr = new FileStream(assetPath, FileMode.Open, FileAccess.Read))
            {
                this.Assets = UndertaleIO.Read(fsr);
                this.CalcEmbeddedTextureHash();
            }
        }

        public UndertaleData Assets { get; } = new UndertaleData();

        public Dictionary<string, int> EmbeddedTextureHashDict { get; } = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

        public IList<int> GetUnknownGlyphs(string fontName, FsFontAtlas jsonFontAtlas)
        {
            var result = new List<int>();

            var assetGlyphs = new SortedSet<int>();
            {
                var selectedFonts = this.Assets.Fonts.Where(x => x.Name.Content == fontName);
                foreach (var font in selectedFonts)
                {
                    foreach (var glyph in font.Glyphs)
                    {
                        var jsonGlyph = new Glyph();
                        assetGlyphs.Add(glyph.Character);
                    }
                }
            }

            var jsonGlyphs = new SortedSet<int>();
            {
                foreach (var aaa in jsonFontAtlas.Glyphs)
                {
                    jsonGlyphs.Add((int)aaa.Value.Character);
                }
            }

            foreach (var character in assetGlyphs)
            {
                if (jsonGlyphs.Contains(character))
                {
                    //// OK
                }
                else
                {
                    //// json に存在しなし
                    result.Add(character);
                }
            }

            return result;
        }

        public int ChangeAtlas(string fontname, FsFontAtlas fontAtlas, int offset)
        {
            //// 指定したフォントを抽出する。
            var selectedFonts = this.Assets.Fonts.Where(x => x.Name.Content == fontname);
            var selectedFontCount = selectedFonts.Count();
            if (selectedFontCount == 0)
            {
                throw new Exception($"Unknown error: フォントが存在しません。");
            }
            else if (selectedFontCount > 1)
            {
                throw new Exception($"Unknown error: フォントが複数存在します。");
            }

            var currentFont = selectedFonts.First();

            //// オリジナルの文字コード一覧を作成
            var caharacterList = new SortedSet<long>();
            foreach (var glyph in currentFont.Glyphs)
            {
                caharacterList.Add(glyph.Character);
            }

            var registeredGlyphs = new SortedDictionary<long, UndertaleFont.Glyph>();

            //// 抽出されたフォントの処理
            currentFont.AntiAliasing = (byte)fontAtlas.AntiAlias;
            currentFont.AscenderOffset = (int)fontAtlas.AscenderOffset + offset;
            currentFont.Bold = fontAtlas.Bold;
            currentFont.Charset = (byte)fontAtlas.Charset;
            ////currentFont.DisplayName = fontAtlas.;
            currentFont.EmSize = (uint)fontAtlas.Size;

            //// 日本語フォントを登録する
            SetJapaneseCharacter(fontAtlas, registeredGlyphs);

            //// オリジナルに存在して日本語フォントに存在しない文字をスペースとして登録する。
            ////SetUnknownCharacter(fontAtlas, currentFont, registeredGlyphs);

            //// グリフを差し替える
            currentFont.Glyphs.Clear();
            var changeResult = ChangeCharacterGlyphs(currentFont, registeredGlyphs);

            currentFont.Italic = fontAtlas.Italic;
            ////currentFont.Name.Content = fontAtlas.Name;
            currentFont.RangeEnd = changeResult.rangeEnd;
            currentFont.RangeStart = changeResult.rangeStart;
            ////currentFont.ScaleX = 1.0f;
            ////currentFont.ScaleY = 1.0f;
            ////currentFont.Texture = fontAtlas;

            return fontAtlas.Glyphs.Count;
        }

        /// <summary>
        /// TexturePageItem の画像を EmbeddedTexture に変更する。
        /// </summary>
        /// <param name="texturePageItemNo">TexturePageItem の番号</param>
        /// <param name="embeddedTextureNo">EmbeddedTexture の番号</param>
        /// <param name="width">画像の幅</param>
        /// <param name="height">画像の高さ</param>
        public void ChangeEmbeddedTextureNoOfTexturePageItem(
            int texturePageItemNo, int embeddedTextureNo, ushort width, ushort height)
        {
            ushort x = 0;
            ushort y = 0;

            //// EmbeddedTexture を取得
            var embeddedTexture = this.Assets.EmbeddedTextures[embeddedTextureNo];
            //// TexturePageItem を取得
            var currentTexturePageItem = this.Assets.TexturePageItems[texturePageItemNo];
            //// TexturePageItem を差し替え
            currentTexturePageItem.TexturePage = embeddedTexture;
            //// Texture の属性を設定
            currentTexturePageItem.BoundingHeight = height;
            currentTexturePageItem.BoundingWidth = width;
            currentTexturePageItem.SourceHeight = height;
            currentTexturePageItem.SourceWidth = width;
            currentTexturePageItem.SourceX = x;
            currentTexturePageItem.SourceY = y;
            currentTexturePageItem.TargetHeight = height;
            currentTexturePageItem.TargetWidth = width;
            currentTexturePageItem.TargetX = x;
            currentTexturePageItem.TargetY = y;
        }

        /// <summary>
        /// データファイル(data.win)にPNGファイルを追加する。
        /// </summary>
        /// <param name="pngPath">PNGファイルのパス</param>
        /// <returns>画像ファイルのサイズ</returns>
        public (int width, int height) AddTexture(string pngPath)
        {
            //// Embedded Texture を作成
            var embeddedTexture = new UndertaleEmbeddedTexture();
            //// PNG形式の画像ファイルを読み込み
            using (var ms = new MemoryStream(TextureWorker.ReadTextureBlob(pngPath)))
            {
                //// 読み込んだ画像データを取り込み
                var pngData = ms.ToArray();
                var pngHashString = GmsHashUtils.CalcHashString(pngData);
                if (this.EmbeddedTextureHashDict.ContainsKey(pngHashString))
                {
                    var currentTextureNo = this.EmbeddedTextureHashDict[pngHashString];
                    var msg =
                        $"Logic error: PNG file already exists. {Environment.NewLine}" +
                        $"Embedded Texture No({currentTextureNo})";
                    throw new Exception(msg);
                }
                else
                {
                    embeddedTexture.TextureData.TextureBlob = pngData;
                }
            }

            //// オリジナルのデータファイルを開く
            //// オリジナルのデータファイルを読み込み
            //// データファイルに Texture を追加する
            this.Assets.EmbeddedTextures.Add(embeddedTexture);

            var width = 0;
            var height = 0;
            using (var bmp = TextureWorker.ReadImageFromFile(pngPath))
            {
#pragma warning disable CA1416 // プラットフォームの互換性を検証
                width = bmp.Width;
                height = bmp.Height;
#pragma warning restore CA1416 // プラットフォームの互換性を検証
            }

            return (width, height);
        }

        public void Save(string path, Action<string>? messageHandler = null)
        {
            //// 出力用データファイルに書き出す
            using (var fsw = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
            {
                UndertaleIO.Write(fsw, this.Assets, x =>
                {
                    messageHandler?.Invoke(x);
                });
            }
        }

        public override string ToString()
        {
            var buff = new StringBuilder();

            buff.AppendLine($"EmbeddedTextures.Count({this.Assets.EmbeddedTextures.Count})");

            return buff.ToString();
        }

        private static (ushort rangeStart, ushort rangeEnd) ChangeCharacterGlyphs(UndertaleFont currentFont, SortedDictionary<long, UndertaleFont.Glyph> registeredGlyphs)
        {
            //// デフォルト文字をスペースのグリフに設定する。
            //// デフォルト文字が存在しないとゲームを起動できない。
            {
                var defaultCharCode = 9647;
                var defaultChar = new UndertaleFont.Glyph();
                var spaceChar = registeredGlyphs[32];
                if (registeredGlyphs.ContainsKey(defaultCharCode))
                {
                    defaultChar = registeredGlyphs[defaultCharCode];

                    defaultChar.Offset = spaceChar.Offset;
                    defaultChar.Shift = spaceChar.Shift;
                    defaultChar.SourceX = spaceChar.SourceX;
                    defaultChar.SourceY = spaceChar.SourceY;
                    defaultChar.SourceHeight = spaceChar.SourceHeight;
                    defaultChar.SourceWidth = spaceChar.SourceWidth;
                }
                else
                {
                    defaultChar.Character = spaceChar.Character;
                    defaultChar.Kerning = spaceChar.Kerning;

                    defaultChar.Offset = spaceChar.Offset;
                    defaultChar.Shift = spaceChar.Shift;
                    defaultChar.SourceX = spaceChar.SourceX;
                    defaultChar.SourceY = spaceChar.SourceY;
                    defaultChar.SourceHeight = spaceChar.SourceHeight;
                    defaultChar.SourceWidth = spaceChar.SourceWidth;

                    registeredGlyphs.Add(defaultChar.Character, defaultChar);
                }
            }

            ushort rangeStart = 0;
            uint rangeEnd = 0;
            foreach (var glyph in registeredGlyphs.Values)
            {
                currentFont.Glyphs.Add(glyph);
                if (rangeStart == 0)
                {
                    rangeStart = glyph.Character;
                }

                rangeEnd = Math.Max(rangeEnd, glyph.Character);
            }

            return (rangeStart, (ushort)rangeEnd);
        }

        private static void SetJapaneseCharacter(FsFontAtlas fontAtlas, SortedDictionary<long, UndertaleFont.Glyph> registeredGlyphs)
        {
            foreach (var glyph in fontAtlas.Glyphs)
            {
                var newGlyph = new UndertaleFont.Glyph();

                newGlyph.Character = (ushort)glyph.Key;
                ////newGlyph.Kerning = ;
                newGlyph.Offset = (short)glyph.Value.Offset;
                newGlyph.Shift = (short)glyph.Value.Shift;
                newGlyph.SourceHeight = (ushort)glyph.Value.H;
                newGlyph.SourceWidth = (ushort)glyph.Value.W;
                newGlyph.SourceX = (ushort)glyph.Value.X;
                newGlyph.SourceY = (ushort)glyph.Value.Y;

                registeredGlyphs.Add(newGlyph.Character, newGlyph);
            }
        }

        private static void SetUnknownCharacter(FsFontAtlas fontAtlas, UndertaleFont currentFont, SortedDictionary<long, UndertaleFont.Glyph> registeredGlyphs)
        {
            var spaceGlyph = fontAtlas.Glyphs[32];
            foreach (var glyph in currentFont.Glyphs)
            {
                if (registeredGlyphs.ContainsKey(glyph.Character))
                {
                    //// すでに登録済みの文字は無視
                }
                else
                {
                    //// オリジナルに存在して、未登録の文字はスペースとして登録する。
                    var newGlyph = new UndertaleFont.Glyph();

                    newGlyph.Character = (ushort)glyph.Character;
                    ////newGlyph.Kerning = ;
                    newGlyph.Offset = (short)spaceGlyph.Value.Offset;
                    newGlyph.Shift = (short)spaceGlyph.Value.Shift;
                    newGlyph.SourceHeight = (ushort)spaceGlyph.Value.H;
                    newGlyph.SourceWidth = (ushort)spaceGlyph.Value.W;
                    newGlyph.SourceX = (ushort)spaceGlyph.Value.X;
                    newGlyph.SourceY = (ushort)spaceGlyph.Value.Y;

                    registeredGlyphs.Add(newGlyph.Character, newGlyph);
                }
            }
        }

        private void CalcEmbeddedTextureHash()
        {
            int textureNo = 0;
            foreach (var embeddedTexture in this.Assets.EmbeddedTextures)
            {
                var textureData = embeddedTexture.TextureData.TextureBlob;
                var hashString = GmsHashUtils.CalcHashString(textureData);
                this.EmbeddedTextureHashDict.Add(hashString, textureNo);

                textureNo++;
            }
        }
    }
}
