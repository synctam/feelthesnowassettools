﻿namespace LibFsTransAid.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;

    public class GmsHashUtils
    {
        public static string CalcHashString(byte[] data)
        {
            var sha = SHA256.Create();
            var hash_sha256 = sha.ComputeHash(data);

            var result = string.Empty;

            for (int i = 0; i < hash_sha256.Length; i++)
            {
                result = result + string.Format("{0:x2}", hash_sha256[i]);
            }

            return result;
        }
    }
}
