﻿namespace LibFsTransAid.FontAtlas
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FsFontSchema;

    public class FsFontAtlasFile
    {
        public Dictionary<int, Glyph> Items { get; } = new Dictionary<int, Glyph>();

        public void AddGlyph(int character, Glyph glyph)
        {
            if (this.Items.ContainsKey(character))
            {
                throw new Exception($"The character already exists. Character({character})");
            }
            else
            {
                this.Items.Add(character, glyph);
            }
        }

        public Glyph? GetGlyph(int character)
        {
            if (this.Items.ContainsKey(character))
            {
                return this.Items[character];
            }
            else
            {
                return null;
            }
        }
    }
}
