﻿namespace LibFsTransAid.FontAtlas
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FsFontSchema;

    public class FsFontAtlasDao
    {
        public static FsFontAtlas LoadFromFile(string path)
        {
            var fontAtlas = FsFontAtlas.FromJson(path);
            return fontAtlas;
        }
    }
}
