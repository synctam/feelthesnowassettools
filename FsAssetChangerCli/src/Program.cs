﻿namespace FsAssetChanger
{
    using System;
    using System.Linq;
    using System.Text;
    using FsFontSchema;
    using LibFsTransAid.ArchiveIO;
    using LibFsTransAid.FontAtlas;
    using S5mDebugTools;

    public class Program
    {
        public static int Main(string[] args)
        {
            ////ExportFont();
            MakeMod();
            {
                ////var assetPath = @"data\10.en\data.win";
                ////var atlasPath = @"data\fonts\jp\JpFont_18p_aa\JpFont_18p_aa.yy";
                ////var fontName = "schi_18";
                ////GetUnknownGlyphs(assetPath, atlasPath, fontName);
            }

            TDebugUtils.Pause();
            return 0;
        }

        internal static void MakeMod()
        {
            var assetPath = @"data\10.en\data.win";
            Console.WriteLine($"Loading data.win({assetPath})...");
            var gmsAssets = new GmsAssets(assetPath);
            Console.WriteLine($"Loaded: data.win.");

            ////
            {
                //// 日本語フォント画像（大）を追加
                var fontid = "JpFont_18p_aa";
                var fontTexturePath = $@"data\fonts\jp\{fontid}\{fontid}.png";
                var texturePageItemNo = 0;
                var embeddedTextureNo = 73;
                var fontName = "schi_18";
                var atlasPath = $@"data\fonts\jp\{fontid}\{fontid}.yy";

                Console.WriteLine();
                Console.WriteLine($"{fontName}");
                var textOffset = 4;
                MakeFont(gmsAssets, fontTexturePath, texturePageItemNo, embeddedTextureNo, fontName, atlasPath, textOffset);
            }

            Console.WriteLine();
            {
                //// 日本語フォント画像(小)を追加
                var fontid = "JpFont_14p_aa";
                var fontTexturePath = $@"data\fonts\jp\{fontid}\{fontid}.png";
                var texturePageItemNo = 77;
                var embeddedTextureNo = 74;
                var fontName = "schi_12";
                var atlasPath = $@"data\fonts\jp\{fontid}\{fontid}.yy";

                Console.WriteLine($"{fontName}");
                var textOffset = 6;
                MakeFont(gmsAssets, fontTexturePath, texturePageItemNo, embeddedTextureNo, fontName, atlasPath, textOffset);
            }

            Console.WriteLine();
            Console.WriteLine($"Saving the data.win...");
            //// 日本語フォントに差し替え済みのデータを出力する。
            var savePath = @"data\20.jp\data.win";
            gmsAssets.Save(savePath, x =>
            {
                Console.WriteLine(x);
            });

            Console.WriteLine($"Saved: data.win({savePath})");
        }

        internal static void MakeFont(
            GmsAssets gmsAssets,
            string fontTexturePath,
            int texturePageItemNo,
            int embeddedTextureNo,
            string fontName,
            string atlasPath,
            int offset)
        {
            //// 日本語フォント画像を追加
            var pngAttr = gmsAssets.AddTexture(fontTexturePath);
            Console.WriteLine($"Added: Embedded Texture({fontTexturePath})");
            //// Texture page item の画像を日本語フォントに変更する
            gmsAssets.ChangeEmbeddedTextureNoOfTexturePageItem(
                texturePageItemNo, embeddedTextureNo, (ushort)pngAttr.width, (ushort)pngAttr.height);
            Console.WriteLine(
                $"Changed: TexturePageItem. TexturePageItemNo({texturePageItemNo}) " +
                $"EmbeddedTextureNo({embeddedTextureNo}) " +
                $"Size({pngAttr.width}x{pngAttr.height})");
            //// フォントの座標情報を差し替える
            var jsonString = File.ReadAllText(atlasPath, Encoding.UTF8);
            var fontAtlas = FsFontAtlas.FromJson(jsonString);
            var characters = gmsAssets.ChangeAtlas(fontName, fontAtlas, offset);
            Console.WriteLine($"Replaced: Glyph({fontName}) Characters({characters})");
        }

        internal static void ExportFont()
        {
            {
                var datawinPath = @"S:\E-Drive\Games\Feel The Snow 日本語化\10.org\data.win";
                var fontName = "schi_18";

                FsAssetDao.ExportFont(datawinPath, fontName, @"data\exported");
            }

            {
                var datawinPath = @"S:\E-Drive\Games\Feel The Snow 日本語化\10.org\data.win";
                var fontName = "schi_12";

                FsAssetDao.ExportFont(datawinPath, fontName, @"data\exported");
            }

            {
                var datawinPath = @"data\20.jp\data.win";
                var fontName = "JpFont_12p_aa";

                FsAssetDao.ExportFont(datawinPath, fontName, @"data\exported");
            }

            {
                var datawinPath = @"data\20.jp\data.win";
                var fontName = "JpFont_18p_aa";

                FsAssetDao.ExportFont(datawinPath, fontName, @"data\exported");
            }
        }

        internal static void GetUnknownGlyphs(string assetPath, string jsonPath, string fontName)
        {
            var gmsAssets = new GmsAssets(assetPath);
            var jsonString = File.ReadAllText(jsonPath);
            var jsonFontAtlas = FsFontAtlasDao.LoadFromFile(jsonString);
            var result = gmsAssets.GetUnknownGlyphs(fontName, jsonFontAtlas);
            var buff = new StringBuilder();
            foreach (var aaa in result)
            {
                buff.AppendLine($"{aaa}");
            }

            File.WriteAllText(@"_UnknownCharacters.txt", buff.ToString());
        }
    }
}